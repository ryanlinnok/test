{
    "name": "Material",
    "version": "0.0.1",
    "category": "Uncategorized",
    "author": "Ryan Linno Kusnandar",
    "website": "https://ryanlinnok.github.io",
    "depends": [
        "base",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/material_view.xml",
        "views/menuitem_material.xml",
        "views/web/material_web_view.xml",
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}
