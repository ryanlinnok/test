window.onload = function() {

    var proyek_dalam_pengerjaan_opt = 0;
    var proyek_belum_dikerjakan_opt = 0;
    var bumn_vol_options = 0;
    var swasta_vol_options = 0;
    var percent_ip_real_vol_options = 0;
    var percent_ip_real_price_options = 0;
    var percent_ip_real_proj_options = 0;
    var single_rmc_options = 0;
    var single_pesaing_options = 0;
    var sharing_options = 0;

    var total_batal_options = 0;
    var total_mundur_options = 0;
    var total_tender_options = 0;
    var total_informasi_options = 0;

    var tahun_options = [];
    var total_volume_options = [];
    var final = [];
    var tahun2018 = 0;
    var tahun2019 = 0;
    var tahun2020 = 0;


      $.ajax({
        url:"/data/get_info_pasar",
        success: function(data,status,jqXHR) {
            var parsedData = JSON.parse(data);

            for(var i = 0; i < parsedData.length;i++){

                for(var z = 0; z < parsedData[i].jenis_pelanggan.length;z++){
                    final.push({
                            name: parsedData[i].total_jenis_pelanggan[z].toLocaleString() + ' m3 ' + parsedData[i].jenis_pelanggan[z],
                            y: parsedData[i].total_jenis_pelanggan[z],
                            sliced: true,
                            selected: true

                    });
                }

                proyek_dalam_pengerjaan_opt = parsedData[i].proyek_dalam_pengerjaan;
                proyek_belum_dikerjakan_opt = parsedData[i].proyek_belum_dikerjakan;

                swasta_vol_options = parsedData[i].vol_swasta;
                bumn_vol_options = parsedData[i].vol_bumn;
                percent_ip_real_vol_options = parsedData[i].percent_ip_real_vol;
                percent_ip_real_price_options = parsedData[i].percent_ip_real_price;
                percent_ip_real_proj_options = parsedData[i].percent_ip_real_proj;
                single_rmc_options = parsedData[i].total_single_rmc;
                single_pesaing_options = parsedData[i].total_single_pesaing;
                sharing_options = parsedData[i].total_sharing;

                total_batal_options = parsedData[i].total_batal;
                total_mundur_options = parsedData[i].total_mundur;
                total_tender_options = parsedData[i].total_tender;
                total_informasi_options = parsedData[i].total_informasi;

                tahun2018 = parsedData[i].total_2018;
                tahun2019 = parsedData[i].total_2019;
                tahun2020 = parsedData[i].total_2020;
                

                tahun_options.push(parsedData[i].tahun);
                // total_volume_options.push(parsedData[i].total_volume_options);
                // total_volume_options = parsedData[i].total_volume;
            }

            var newprogress1=percent_ip_real_vol_options; //ganti ini dari variabel ajax
            $('#progressbar1').width(newprogress1 + "%").attr('aria-valuenow', newprogress1);

            var newprogress2=percent_ip_real_price_options; //ganti ini dari variabel ajax
            $('#progressbar2').width(newprogress2 + "%").attr('aria-valuenow', newprogress2);

            var newprogress3=percent_ip_real_proj_options; //ganti ini dari variabel ajax
            $('#progressbar3').width(newprogress3 + "%").attr('aria-valuenow', newprogress3);

    Highcharts.chart('container1', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        // chart: {
        //     plotBackgroundColor: null,
        //     plotBorderWidth: null,
        //     plotShadow: false,
        //     type: 'pie'
        // },
        title: {
            text: 'Info Pasar Berdasarkan Pelaksanaanya'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: proyek_dalam_pengerjaan_opt.toLocaleString() + ' m3 Dalam Pengerjaan',
                y: proyek_dalam_pengerjaan_opt,
                sliced: true,
                selected: true
            },{
                name: proyek_belum_dikerjakan_opt.toLocaleString() + ' m3 Belum Dikerjakan',
                y: proyek_belum_dikerjakan_opt,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    Highcharts.chart('container2', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Info Pasar Berdasarkan Pelanggan'
        },
        tooltip: {
            pointFormat: '{series.name} : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name} </b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            name: 'Percentage',
            colorByPoint: true,
            data: final
        }]
        })

    Highcharts.chart('container3', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Info Pasar Dalam Pengerjaan'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            colorByPoint: true,
            data: [
            {
                name: single_rmc_options.toLocaleString() + ' m3 Single RMC',
                y: single_rmc_options,
                sliced: true,
                selected: true
            },{
                name: single_pesaing_options.toLocaleString() + ' m3 Single Pesaing',
                y: single_pesaing_options,
                sliced: true,
                selected: true
            },
            {
                name: sharing_options.toLocaleString() + ' m3 Sharing',
                y: sharing_options,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    Highcharts.chart('container4', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Info Pasar Belum Dikerjakan'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: total_batal_options.toLocaleString() + ' m3 Batal',
                y: total_batal_options,
                sliced: true,
                selected: true
            },{
                name: total_mundur_options.toLocaleString() + ' m3 Mundur',
                y: total_mundur_options,
                sliced: true,
                selected: true
            },
            {
                name: total_tender_options.toLocaleString() + ' m3 Tender',
                y: total_tender_options,
                sliced: true,
                selected: true
            },{
                name: total_informasi_options.toLocaleString() + ' m3 Informasi',
                y: total_informasi_options,
                sliced: true,
                selected: true
            },
            ]
        }]
        })
    Highcharts.chart('container', {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },

            title: {
                text: 'Info Pasar Berdasarkan Waktu Mulai Proyek'
            },
            tooltip: {
                pointFormat: 'Total Volume : <b>{point.y:.0f} m3</b>'
            },

            subtitle: {
                text: ''
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            credits: {
                  enabled: false
            },
            xAxis: {
                categories: ['Tahun 2018','Tahun 2019','Tahun 2020'],
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: '16px'
                    }
                },
                title: {
                    text: 'Tahun'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Volume (M3)'
                }
            },
            showInLegend: false,
            series: [{
                // colorByPoint: true,
                name: 'Volume Info Pasar',
                data: [tahun2018,tahun2019,tahun2020]
            }]

        });

    }
});

        }


