window.onload = function() {

    var data_piutang = [];
    var data_piutang_cara_bayar = [];
    var data_piutang_skbdn = [];
    var data_piutang_scf = [];
    var data_piutang_giro = [];

    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;

  $.ajax({
    url:"/data/keuangan/get_piutang/",
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].data_piutang.length;z++){
        		data_piutang.push(parsedData[i].data_piutang[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_cara_bayar.length;z++){
        		data_piutang_cara_bayar.push(parsedData[i].data_piutang_cara_bayar[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_skbdn.length;z++){
        		data_piutang_skbdn.push(parsedData[i].data_piutang_skbdn[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_scf.length;z++){
        		data_piutang_scf.push(parsedData[i].data_piutang_scf[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_giro.length;z++){
        		data_piutang_giro.push(parsedData[i].data_piutang_giro[z]);	
        	}

        }

    // Highcharts.setOptions({
    //  colors: ['#50B432','#ED561B']
    // });


    // PIUTANG BERDASARKAN PROSES PIUTANG
	Highcharts.chart('container_piutang_by_process', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'Piutang Berdasarkan Proses Piutang'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['Docket blm Kwitansi : Rp. ' + data_piutang[0].toString() + ' M', data_piutang[0]],
	            ['Kuitansi (data tdk lengkap) : Rp. ' + data_piutang[1].toString() + ' M', data_piutang[1]],
	            ['Kuitansi (data lengkap) : Rp. ' + data_piutang[2].toString() + ' M', data_piutang[2]],
	            ['Blm jatuh tempo : Rp. ' + data_piutang[3].toString() + ' M', data_piutang[3]],
	            ['Sudah jatuh tempo : Rp. ' + data_piutang[4].toString() + ' M', data_piutang[4]]
	        ],
	        showInLegend: true
	    }]
	});

    // PIUTANG BERDASARKAN CARA BAYAR
	Highcharts.chart('container_piutang_by_payment_term', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'Piutang Berdasarkan Cara Bayar'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['SKBDN : Rp. ' + data_piutang_cara_bayar[0].toString() + ' M', data_piutang_cara_bayar[0]],
	            ['SCF : Rp. ' + data_piutang_cara_bayar[1].toString() + ' M', data_piutang_cara_bayar[1]],
	            ['Pembayaran dimuka : Rp. ' + data_piutang_cara_bayar[4].toString() + ' M', data_piutang_cara_bayar[4]],
	            ['Giro : Rp. ' + data_piutang_cara_bayar[2].toString() + ' M', data_piutang_cara_bayar[2]],
	            ['Konvensional : Rp. ' + data_piutang_cara_bayar[3].toString() + ' M', data_piutang_cara_bayar[3]]
	        ],
	        showInLegend: true
	    }]
	});


    // PIUTANG VIA SWIFT SKBDN
	Highcharts.chart('container_piutang_swift_skbdn', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoring Piutang via Swift SKBDN'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_skbdn[0],0,0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_skbdn[1],0,0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_skbdn[2],0,0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_skbdn[3],0,0]
	    },{
	        name: 'Swift Terbit',
	        data: [0,0,0,0,data_piutang_skbdn[4],0]
	    },{
	        name: 'Swift Aktif',
	        data: [0,0,0,0,0,data_piutang_skbdn[5]]
	    },
	    ]
	});
	// PIUTANG VIA GIRO
	Highcharts.chart('container_piutang_giro', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoring Piutang via Giro'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_giro[0],0,0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_giro[1],0,0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_giro[2],0,0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_giro[3],0,0]
	    },{
	        name: 'Giro Terbit',
	        data: [0,0,0,0,data_piutang_giro[4],0]
	    },{
	        name: 'Giro Aktif',
	        data: [0,0,0,0,0,data_piutang_giro[5]]
	    },
	    ]
	});

	// PIUTANG VIA SCF
	Highcharts.chart('container_piutang_scf', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoging Piutang via SCF'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_scf[0],0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_scf[1],0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_scf[2],0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_scf[3],0]
	    },{
	        name: 'SCF Terbit',
	        data: [0,0,0,0,data_piutang_scf[4]]
	    }
	    ]
	});


    Highcharts.setOptions({
     colors: ['#2CAFED','#FFA62D','#0CCB2C']
    });

    // UMUR PIUTANG DALAM PROSES
	Highcharts.chart('container_umur_piutang_dalam_proses', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>UMUR PIUTANG DALAM PROSES</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1 Hari', '2 Hari', '3 Hari', '4 Hari', '5 Hari', '6 Hari', '>7 Hari']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Docket Belum Invoices',
	        data: [25,17,24,11,7,5,40]
	    },{
	        name: 'Draft Invoices',
	        data: [50,30,30,27,20,10,10]
	    }, {
	        name: 'Open Invoices',
	        data: [0,20,17,31,20,30,35]
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // UMUR PIUTANG USAHA
	Highcharts.chart('container_umur_piutang_usaha', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>UMUR PIUTANG USAHA</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1-7', '8-14', '15-30', '31-60', '61-90','91-120','121-180','181-360','>361']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: [0,2,59,81,30,13,0,0,0]
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: [0,0,0,20,30,45,47,97,5]
	    }]
	});

    
    Highcharts.setOptions({
     colors: ['#00cc66','#ffd11a','#ff5c33','#cc2900','#000000']
    });

    // PENGENDALIAN PLAFON KREDIT PIUTANG
	Highcharts.chart('container_plafon_kredit_piutang', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>PENGENDALIAN PLAFON KREDIT PIUTANG</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['AMAN','HATI-HATI','OVER','STOP','BLACKLIST'],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 25,
	        floating: false,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            },
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'AMAN',
	        data: [20,null,null,null,null]
	    }, {
	        name: 'HATI-HATI',
	        data: [null,50,null,null,null]
	    }, {
	        name: 'OVER',
	        data: [null,null,25,null,null]
	    },{
	        name: 'STOP',
	        data: [null,null,null,5,null]
	    },{
	        name: 'BLACKLIST',
	        data: [null,null,null,null,9]
	    }]
	});


    Highcharts.setOptions({
     colors: ['#2CAFED','#FFA62D']
    });

    // FINAL ACCOUNT - JUMLAH PROYEK
	Highcharts.chart('container_final_account_jumlah_proyek', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'JUMLAH PROYEK'
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total',
	        data: [
	            ['Kontrak On Going : 200 Proyek', 200],
	            ['Kontrak Selesai : 50 Proyek', 50]
	        ],
	        showInLegend: true
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#FFA62D']
    });

    // FINAL ACCOUNT - STATUS FINAL ACCOUNT
	Highcharts.chart('container_final_account_status', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'STATUS FINAL ACCOUNT'
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total',
	        data: [
	            ['Ada Final Account : 32 Kontrak', 32],
	            ['Tidak Ada Final Account : 18 Kontrak', 18]
	        ],
	        showInLegend: true
	    }]
	});

//END
    }
});
    
}



function onchange_periode(val) {

	if(val == 'daily'){
		document.getElementById('sel_harian_start').style.visibility = "visible";
		document.getElementById('sel_harian_end').style.visibility = "visible";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";
	}
    else if(val == 'monthly'){
		document.getElementById('sel_harian_start').style.visibility = "hidden";
		document.getElementById('sel_harian_end').style.visibility = "hidden";
		document.getElementById('sel_bulanan_start').style.visibility = "visible";
		document.getElementById('sel_bulanan_end').style.visibility = "visible";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";

    }
    else if(val == 'yearly'){
		document.getElementById('sel_harian_start').style.visibility = "hidden";
		document.getElementById('sel_harian_end').style.visibility = "hidden";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "visible";
		document.getElementById('sel_tahunan_end').style.visibility = "visible";
    }
	else {
		document.getElementById('sel_harian_start').style.visibility = "visible";
		document.getElementById('sel_harian_end').style.visibility = "visible";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";
	}
    
}


function tampilkan(val) {
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	today2 = yyyy + '-' + mm + '-' + dd;
	organisasi = document.getElementById('organisasi').value;

    var data_piutang = [];
    var data_piutang_cara_bayar = [];
    var data_piutang_skbdn = [];
    var data_piutang_scf = [];
    var data_piutang_giro = [];
    var data_piutang_a = [];
    var data_piutang_b = [];
    var data_piutang_c = [];
    var data_piutang_belum_jatuh_tempo = [];
    var data_piutang_sudah_jatuh_tempo = [];
    
  $.ajax({
    url:"/data/keuangan/get_piutang/" + today2,
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].data_piutang.length;z++){
        		data_piutang.push(parsedData[i].data_piutang[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_cara_bayar.length;z++){
        		data_piutang_cara_bayar.push(parsedData[i].data_piutang_cara_bayar[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_skbdn.length;z++){
        		data_piutang_skbdn.push(parsedData[i].data_piutang_skbdn[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_scf.length;z++){
        		data_piutang_scf.push(parsedData[i].data_piutang_scf[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_giro.length;z++){
        		data_piutang_giro.push(parsedData[i].data_piutang_giro[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_a.length;z++){
        		data_piutang_a.push(parsedData[i].data_piutang_a[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_b.length;z++){
        		data_piutang_b.push(parsedData[i].data_piutang_b[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_c.length;z++){
        		data_piutang_c.push(parsedData[i].data_piutang_c[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_belum_jatuh_tempo.length;z++){
        		data_piutang_belum_jatuh_tempo.push(parsedData[i].data_piutang_belum_jatuh_tempo[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_piutang_sudah_jatuh_tempo.length;z++){
        		data_piutang_sudah_jatuh_tempo.push(parsedData[i].data_piutang_sudah_jatuh_tempo[z]);	
        	}

        }

    Highcharts.setOptions({
     colors: ['#00cc66','#F39C12','#A569BD','#228CF5','#F54822']
    });


    // PIUTANG BERDASARKAN PROSES PIUTANG
	Highcharts.chart('container_piutang_by_process', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'Piutang Berdasarkan Proses Piutang'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['Docket Belum Kwitansi : ' + data_piutang[0].toString() + 'M', data_piutang[0]],
	            ['Kuitansi (Tidak lengkap) : ' + data_piutang[1].toString() + ' M', data_piutang[1]],
	            ['Kuitansi (Lengkap) : ' + data_piutang[2].toString() + 'M', data_piutang[2]],
	            ['Belum Jatuh tempo : ' + data_piutang[3].toString() + 'M', data_piutang[3]],
	            ['Sudah Jatuh tempo : ' + data_piutang[4].toString() + 'M', data_piutang[4]]
	        ],
	        showInLegend: true
	    }]
	});

    Highcharts.setOptions({
     colors: ['#00cc66','#F39C12','#A569BD','#228CF5','#F54822']
    });

    // PIUTANG BERDASARKAN CARA BAYAR
	Highcharts.chart('container_piutang_by_payment_term', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: 'Piutang Berdasarkan Cara Bayar'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['SKBDN : ' + data_piutang_cara_bayar[0].toString() + 'M', data_piutang_cara_bayar[0]],
	            ['SCF : ' + data_piutang_cara_bayar[1].toString() + 'M', data_piutang_cara_bayar[1]],
	            ['Pembayaran dimuka : ' + data_piutang_cara_bayar[4].toString() + 'M', data_piutang_cara_bayar[4]],
	            ['Giro : ' + data_piutang_cara_bayar[2].toString() + 'M', data_piutang_cara_bayar[2]],
	            ['Konvensional : ' + data_piutang_cara_bayar[3].toString() + 'M', data_piutang_cara_bayar[3]]
	        ],
	        showInLegend: true
	    }]
	});


    Highcharts.setOptions({
     colors: ['#00cc66','#F39C12','#A569BD','#228CF5','#F54822','#808080']
    });


    // PIUTANG VIA SWIFT SKBDN
	Highcharts.chart('container_piutang_swift_skbdn', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoring Piutang via Swift SKBDN'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_skbdn[0],0,0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_skbdn[1],0,0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_skbdn[2],0,0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_skbdn[3],0,0]
	    },{
	        name: 'Swift Terbit',
	        data: [0,0,0,0,data_piutang_skbdn[4],0]
	    },{
	        name: 'Swift Aktif',
	        data: [0,0,0,0,0,data_piutang_skbdn[5]]
	    },
	    ]
	});
	// PIUTANG VIA GIRO
	Highcharts.chart('container_piutang_giro', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoring Piutang via Giro'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_giro[0],0,0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_giro[1],0,0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_giro[2],0,0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_giro[3],0,0]
	    },{
	        name: 'Giro Terbit',
	        data: [0,0,0,0,data_piutang_giro[4],0]
	    },{
	        name: 'Giro Aktif',
	        data: [0,0,0,0,0,data_piutang_giro[5]]
	    },
	    ]
	});

	// PIUTANG VIA SCF
	Highcharts.chart('container_piutang_scf', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Monitoging Piutang via SCF'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['','','','','','']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: ''
	        }
	    },
	    legend: {
	        layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: true,
	    },
	    plotOptions: {
	        series: {
	            stacking: 'normal'
	        },
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Kontrak',
	        data: [data_piutang_scf[0],0,0,0,0]
	    },{
	        name: 'Sales',
	        data: [0,data_piutang_scf[1],0,0,0]
	    },{
	        name: 'Piutang',
	        data: [0,0,data_piutang_scf[2],0,0]
	    },{
	        name: 'Sisa Kontrak',
	        data: [0,0,0,data_piutang_scf[3],0]
	    },{
	        name: 'SCF Terbit',
	        data: [0,0,0,0,data_piutang_scf[4]]
	    }
	    ]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#FFA62D','#0CCB2C']
    });

    // UMUR PIUTANG DALAM PROSES
	Highcharts.chart('container_umur_piutang_dalam_proses', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>UMUR PIUTANG DALAM PROSES</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1 Hari', '2 Hari', '3 Hari', '4 Hari', '5 Hari', '6 Hari', '>7 Hari']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Docket Belum Invoices',
	        data: data_piutang_a
	    },{
	        name: 'Draft Invoices',
	        data: data_piutang_b
	    }, {
	        name: 'Open Invoices',
	        data: data_piutang_c
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // UMUR PIUTANG USAHA
	Highcharts.chart('container_umur_piutang_usaha', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>UMUR PIUTANG USAHA</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1-7', '8-14', '15-30', '31-60', '61-90','91-120','121-180','181-360','>361']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: data_piutang_belum_jatuh_tempo
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: data_piutang_sudah_jatuh_tempo
	    }]
	});



//END
    }
});
    
}




