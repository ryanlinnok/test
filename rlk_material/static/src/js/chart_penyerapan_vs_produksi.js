window.onload = function() {

    var list_penyerapan_value = [];
    var list_produksi_value = [];
    var list_pencapaian_value = [];
    var list_penyerapan_value_kum = [];
    var list_produksi_value_kum = [];
    var list_pencapaian_value_kum = [];

    $.ajax({
        url:"/data/get_penyerapan_vs_produksi",
        success: function(data,status,jqXHR) {
            var parsedData = JSON.parse(data);
    
            for(var i = 0; i < parsedData.length;i++){
                list_penyerapan_value.push(parsedData[i].total_penyerapan);
                list_produksi_value.push(parsedData[i].total_realisasi);
                list_pencapaian_value.push(parsedData[i].total_pencapaian);
                list_penyerapan_value_kum.push(parsedData[i].kum_total_penyerapan);
                list_produksi_value_kum.push(parsedData[i].kum_total_realisasi);
                list_pencapaian_value_kum.push(parsedData[i].kum_total_pencapaian);
            }

        Highcharts.chart('container', {
            chart: {
                zoomType: 'xy',
                width:1200
            },
            title: {
                text: 'Rencana Penyerapan VS Realisasi Produksi'
            },
            subtitle: {
                text: '-- Bulanan --'
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'Mei', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                crosshair: true
            },
            credits: {
                  enabled: false
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} m3',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Volume',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Persentase Pencapaian',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}%',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'Rencana Penyerapan',
                type: 'column',
                data: list_penyerapan_value,
                tooltip: {
                    valueSuffix: ' m3'
                }
            }, {
                name: 'Realisasi Produksi',
                type: 'column',
                data: list_produksi_value,
                tooltip: {
                    valueSuffix: ' m3'
                }

            }, {
                name: 'Persentase Pencapaian',
                type: 'spline',
                yAxis: 1,
                data: list_pencapaian_value,
                tooltip: {
                    valueSuffix: '%'
                }
            }
            ]
        });

        Highcharts.chart('container_kumulatif', {
            chart: {
                zoomType: 'xy',
                width:1200
            },
            title: {
                text: 'Rencana Penyerapan VS Realisasi Produksi'
            },
            subtitle: {
                text: '-- Kumulatif --'
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'Mei', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                crosshair: true
            },
            credits: {
                  enabled: false
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} m3',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Volume',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Persentase Pencapaian',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}%',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'Rencana Penyerapan',
                type: 'column',
                data: list_penyerapan_value_kum,
                tooltip: {
                    valueSuffix: ' m3'
                }
            }, {
                name: 'Realisasi Produksi',
                type: 'column',
                data: list_produksi_value_kum,
                tooltip: {
                    valueSuffix: ' m3'
                }

            }, {
                name: 'Persentase Pencapaian',
                type: 'spline',
                yAxis: 1,
                data: list_pencapaian_value_kum,
                tooltip: {
                    valueSuffix: '%'
                }
            }
            ]
        });

        }

    });


}
