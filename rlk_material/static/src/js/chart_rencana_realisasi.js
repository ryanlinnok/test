window.onload = function() {

    var pic_options = [];
    var rencana_options1 = [];
    var rencana_options2 = [];
    var realisasi_options1 = [];
    var realisasi_options2 = [];

      $.ajax({
        url:"/data/get_rencana_realisasi",
        success: function(data,status,jqXHR) {
            var parsedData = JSON.parse(data);
    
            for(var i = 0; i < parsedData.length;i++){
    
                pic_options.push(parsedData[i].name);
                rencana_options1.push(parsedData[i].total_rencana1);
                rencana_options2.push(parsedData[i].total_rencana2);
                realisasi_options1.push(parsedData[i].total_realisasi1);
                realisasi_options2.push(parsedData[i].total_realisasi2);
    
            }

    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'RENCANA VS REALISASI PRODUKSI'
    },
    xAxis: {
        categories: pic_options,
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Total (m3)'
        }
    }, {
        title: {
            text: ''
        },
        opposite: true
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0
        }
    },
    credits: {
          enabled: false
      },
        series: [{
        // name: 'Realisasi s/d Saat Ini',
        color: 'rgba(165,170,217,1)',
        // data: [150, 73, 20],
        data: realisasi_options1,
        tooltip: {
            // valuePrefix: '$',
            valueSuffix: ' m3'
        },
        pointPadding: 0.3,
        pointPlacement: -0.2
    }, {
        name: 'Rencana s/d Bulan Ini',
        color: 'rgba(126,86,134,.9)',
        // data: [140, 90, 40],
        data: rencana_options1,
        tooltip: {
            // valuePrefix: '$',
            valueSuffix: ' m3'
        },
        pointPadding: 0.4,
        pointPlacement: -0.2
    }, {
        name: 'Proyeksi s/d Akhir Tahun',
        color: 'rgba(248,161,63,1)',
        data: realisasi_options2,
        // data: [183.6, 178.8, 198.5],
        tooltip: {
            // valuePrefix: '$',
            valueSuffix: ' m3'
        },
        pointPadding: 0.3,
        pointPlacement: 0.2,
        // yAxis: 1
    }, {
        name: 'Rencana s/d Akhir Tahun',
        color: 'rgba(186,60,61,.9)',
        data: rencana_options2,
        // data: [203.6, 198.8, 208.5],
        tooltip: {
            // valuePrefix: '$',
            valueSuffix: ' m3'
        },
        pointPadding: 0.4,
        pointPlacement: 0.2,
        // yAxis: 1
    }]
    
},function(chart){
        
    $('#display_all_plant').click(function(){
        
        chart.showLoading();
        var pic_options = [];
        var rencana_options1 = [];
        var rencana_options2 = [];
        var realisasi_options1 = [];
        var realisasi_options2 = [];

        var url = "/data/get_rencana_realisasi_all";
        $.getJSON(url, function (parsedData) {
            for(var i = 0; i < parsedData.length;i++){
                pic_options.push(parsedData[i].name);
                rencana_options1.push(parsedData[i].total_rencana1);
                rencana_options2.push(parsedData[i].total_rencana2);
                realisasi_options1.push(parsedData[i].total_realisasi1);
                realisasi_options2.push(parsedData[i].total_realisasi2);
            }
            
            chart.hideLoading();

            chart.xAxis[0].update({
                categories:pic_options
            }); 
            
            chart.series[0].update({
                data:realisasi_options1
            });

            chart.series[1].update({
                data:rencana_options1
            });

            chart.series[2].update({
                data:realisasi_options2
            });

            chart.series[3].update({
                data:rencana_options2
            });

        });

    });
        }
    );
    }
});

        }
