window.onload = function() {

    var semen_available = 0;
    var semen_not_available = 0;
    var pasir_available = 0;
    var pasir_not_available = 0;
    var fly_ash_available = 0;
    var fly_ash_not_available = 0;
    var abu_batu_available = 0;
    var abu_batu_not_available = 0;
    var split_available = 0;
    var split_not_available = 0;
    var admixture_available = 0;
    var admixture_not_available = 0;

  $.ajax({
    url:"/data/produksi/get_status_stock_material",
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){

            semen_available = parsedData[i].semen_available;
            semen_not_available = parsedData[i].semen_not_available;

            pasir_available = parsedData[i].pasir_available;
            pasir_not_available = parsedData[i].pasir_not_available;

            fly_ash_available = parsedData[i].fly_ash_available;
            fly_ash_not_available = parsedData[i].fly_ash_not_available;

            abu_batu_available = parsedData[i].abu_batu_available;
            abu_batu_not_available = parsedData[i].abu_batu_not_available;

            split_available = parsedData[i].split_available;
            split_not_available = parsedData[i].split_not_available;

            admixture_available = parsedData[i].admixture_available;
            admixture_not_available = parsedData[i].admixture_not_available;
        }

    Highcharts.setOptions({
     colors: ['#50B432','#ED561B']
    });

    // SEMEN
    Highcharts.chart('container_semen', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'SEMEN'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: semen_available.toLocaleString() + ' PLANT AVAILABLE',
                y: semen_available,
                sliced: true,
                selected: true
            },{
                name: semen_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: semen_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    // FLY ASH
    Highcharts.chart('container_fly_ash', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'FLY ASH'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: fly_ash_available.toLocaleString() + ' PLANT AVAILABLE',
                y: fly_ash_available,
                sliced: true,
                selected: true
            },{
                name: fly_ash_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: fly_ash_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    // PASIR
    Highcharts.chart('container_pasir', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'PASIR'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: pasir_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: pasir_available,
                sliced: true,
                selected: true
            },{
                name: pasir_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: pasir_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    // SPLIT
    Highcharts.chart('container_split', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'SPLIT'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: split_available.toLocaleString() + ' PLANT AVAILABLE',
                y: split_available,
                sliced: true,
                selected: true
            },{
                name: split_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: split_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    // ABU BATU
    Highcharts.chart('container_abu_batu', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'ABU BATU'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: abu_batu_available.toLocaleString() + ' PLANT AVAILABLE',
                y: abu_batu_available,
                sliced: true,
                selected: true
            },{
                name: abu_batu_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: abu_batu_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })

    // ADMIXTURE
    Highcharts.chart('container_admixture', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        
        title: {
            text: 'ADMIXTURE'
        },
        tooltip: {
            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>',
                    // format: '<b>{point.name}</b> <br/>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        credits: {
              enabled: false
        },
        series: [{
            type: 'pie',
            // name: 'Percentage',
            colorByPoint: true,
            data: [
            {
                name: admixture_available.toLocaleString() + ' PLANT AVAILABLE',
                y: admixture_available,
                sliced: true,
                selected: true
            },{
                name: admixture_not_available.toLocaleString() + ' PLANT NOT AVAILABLE',
                y: admixture_not_available,
                sliced: true,
                selected: true
            },
            ]
        }]
        })


//END
    }
});
}


