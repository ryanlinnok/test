window.onload = function() {

    var proyek_dalam_pengerjaan_opt = 0;
    var proyek_belum_dikerjakan_opt = 0;
    var bumn_vol_options = 0;
    var final = [];

      $.ajax({
        url:"/data/get_info_pasar",
        success: function(data,status,jqXHR) {
            var parsedData = JSON.parse(data);

            for(var i = 0; i < parsedData.length;i++){

                for(var z = 0; z < parsedData[i].jenis_pelanggan.length;z++){
                    final.push({
                            name: parsedData[i].total_jenis_pelanggan[z].toLocaleString() + ' m3 ' + parsedData[i].jenis_pelanggan[z],
                            y: parsedData[i].total_jenis_pelanggan[z],
                            sliced: true,
                            selected: true

                    });
                }

                proyek_dalam_pengerjaan_opt = parsedData[i].proyek_dalam_pengerjaan;
                proyek_belum_dikerjakan_opt = parsedData[i].proyek_belum_dikerjakan;
            }

        // PUSAT
        Highcharts.setOptions({
        chart: {
            inverted: true,
            marginLeft: 135,
            type: 'bullet'
        },
        title: {
            text: null
        },
        legend: {
            enabled: false
        },
        yAxis: {
            gridLineWidth: 0
        },
        plotOptions: {
            series: {
                pointPadding: 0.25,
                borderWidth: 0,
                color: '#A569BD',
                targetOptions: {
                    width: '200%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        }
    });

    Highcharts.chart('container1', {
        chart: {
            marginTop: 40
        },
        title: {
            text: 'PUSAT'
        },
        xAxis: {
            categories: ['<span class="hc-cat-title">Realisasi Volume</span><br/>51.0650 m3']
        },
        yAxis: {
            plotBands: [{
                from: 0,
                to: 1150000,
                color: '#EC7063'
            }, {
                from: 1150001,
                to: 1150000+1400000,
                color: '#5DADE2'
            }],
            title: null
        },
        series: [{
            data: [{
                y: 510650,
                target: 0
            }]
        }],
        tooltip: {
            pointFormat: '<b>{point.y}</b> (with target at {point.target})'
        }
    });

    // DIVISI 1
        Highcharts.setOptions({
        chart: {
            inverted: true,
            marginLeft: 135,
            type: 'bullet'
        },
        title: {
            text: null
        },
        legend: {
            enabled: false
        },
        yAxis: {
            gridLineWidth: 0
        },
        plotOptions: {
            series: {
                pointPadding: 0.25,
                borderWidth: 0,
                color: '#A569BD',
                targetOptions: {
                    width: '200%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        }
    });

    Highcharts.chart('container2', {
        chart: {
            marginTop: 40
        },
        title: {
            text: 'Divisi 1'
        },
        xAxis: {
            categories: ['<span class="hc-cat-title">Realisasi Volume</span><br/>612.000 m3']
        },
        yAxis: {
            plotBands: [{
                from: 0,
                to: 295700,
                color: '#EC7063'
            }, {
                from: 295701,
                to: 295700+512000,
                color: '#5DADE2'
            }],
            title: null
        },
        series: [{
            data: [{
                y: 612000,
                target: 0
            }]
        }],
        tooltip: {
            pointFormat: '<b>{point.y}</b> (with target at {point.target})'
        }
    });

    // DIVISI 2
        Highcharts.setOptions({
        chart: {
            inverted: true,
            marginLeft: 135,
            type: 'bullet'
        },
        title: {
            text: null
        },
        legend: {
            enabled: false
        },
        yAxis: {
            gridLineWidth: 0
        },
        plotOptions: {
            series: {
                pointPadding: 0.25,
                borderWidth: 0,
                color: '#A569BD',
                targetOptions: {
                    width: '200%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        }
    });

    Highcharts.chart('container3', {
        chart: {
            marginTop: 40
        },
        title: {
            text: 'Divisi 2'
        },
        xAxis: {
            categories: ['<span class="hc-cat-title">Realisasi Volume</span><br/>568.000 m3']
        },
        yAxis: {
            plotBands: [{
                from: 0,
                to: 310500,
                color: '#EC7063'
            }, {
                from: 310501,
                to: 310500+512000,
                color: '#5DADE2'
            }],
            title: null
        },
        series: [{
            data: [{
                y: 568000,
                target: 0
            }]
        }],
        tooltip: {
            pointFormat: '<b>{point.y}</b> (with target at {point.target})'
        }
    });

    // DIVISI 3
    Highcharts.setOptions({
    chart: {
        inverted: true,
        marginLeft: 135,
        type: 'bullet'
    },
    title: {
        text: null
    },
    legend: {
        enabled: false
    },
    yAxis: {
        gridLineWidth: 0
    },
    plotOptions: {
        series: {
            pointPadding: 0.25,
            borderWidth: 0,
            color: '#A569BD',
            targetOptions: {
                width: '200%'
            }
        }
    },
    credits: {
        enabled: false
    },
    exporting: {
        enabled: false
    }
});

Highcharts.chart('container4', {
    chart: {
        marginTop: 40
    },
    title: {
        text: 'DIVISI 3'
    },
    xAxis: {
        categories: ['<span class="hc-cat-title">Volume</span><br/>220.000 m3']
    },
    yAxis: {
        plotBands: [{
            from: 0,
            to: 20000,
            color: '#EC7063'
        }, {
            from: 20001,
            to: 20000+120000,
            color: '#5DADE2'
        }],
        title: null
    },
    series: [{
        data: [{
            y: 220000,
            target: 0
        }]
    }],
    tooltip: {
        pointFormat: '<b>{point.y}</b> (with target at {point.target})'
    }
});


    }
});

        }


