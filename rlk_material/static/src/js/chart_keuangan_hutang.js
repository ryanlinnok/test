window.onload = function() {

    var data_hutang_umum = [];
    var data_hutang_bank = [];

    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;

  $.ajax({
    url:"/data/keuangan/get_hutang/",
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].data_hutang_umum.length;z++){
        		data_hutang_umum.push(parsedData[i].data_hutang_umum[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_hutang_bank.length;z++){
        		data_hutang_bank.push(parsedData[i].data_hutang_bank[z]);	
        	}

        }

    // Highcharts.setOptions({
    //  colors: ['#2CAFED','#FFA62D','#0CCB2C','ED561B']
    // });

    // HUTANG UMUM
	Highcharts.chart('container_hutang_umum', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: '<strong>HUTANG</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['Bank : ' + data_hutang_umum[0].toString() + 'M', data_hutang_umum[0]],
	            ['Vendor : ' + data_hutang_umum[1].toString() + 'M', data_hutang_umum[1]],
	            ['Beban hrs dibayar : ' + data_hutang_umum[2].toString() + 'M', data_hutang_umum[2]],
	            ['Pajak : ' + data_hutang_umum[3].toString() + 'M', data_hutang_umum[3]]
	        ],
	        showInLegend: true
	    }]
	});

    // HUTANG BANK
	Highcharts.chart('container_hutang_bank', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: '<strong>HUTANG BANK</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['SKBDN : ' + data_hutang_bank[0].toString() + 'M', data_hutang_bank[0]],
	            ['AR : ' + data_hutang_bank[1].toString() + 'M', data_hutang_bank[1]],
	            ['Investasi : ' + data_hutang_bank[2].toString() + 'M', data_hutang_bank[2]]
	        ],
	        showInLegend: true
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // HUTANG VENDOR AGING
	Highcharts.chart('container_hutang_vendor_aging', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>MONITORING AGING HUTANG VENDOR</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1-7', '8-14', '15-30', '31-60', '61-90','91-120','121-180','181-360','>361']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: [2.2,1,0.3,0.8,0.5,0,0,0,0]
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: [0,0.2,1.4,1.6,1.8,1.1,0.2,0.2,1]
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // HUTANG VENDOR AGING DETAIL (PER VENDOR)
	Highcharts.chart('container_hutang_vendor_aging_detail', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	        // text: '<strong>MONITORING AGING HUTANG BY VENDOR</strong>'
	    },
	    // subtitle: {
	    //     text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    // },
	    xAxis: {
	        categories: ['1-7 Hari', '8-14 Hari', '15-30 Hari', '31-60 Hari', '61-90 Hari','91-120 Hari','121-180 Hari','181-360 Hari','>361 Hari']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: [0,2,59,81,30,13,0,0,0]
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: [0,0,0,20,30,45,47,97,5]
	    }]
	});



//END
    }
});
    
}




function tampilkan(val) {
	// var period = document.getElementById('sel1').value;
	var period = 'daily';
	var data_start = 'none';
	var data_end = 'none';
	
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;

    var data_hutang_umum = [];
    var data_hutang_bank = [];
    var data_hutang_vendor = [];
    var data_hutang_vendor_due = [];

  $.ajax({
    url:"/data/keuangan/get_hutang/" + period.toString() + "/" + data_start.toString() + "/" + data_end.toString(),
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].data_hutang_umum.length;z++){
        		data_hutang_umum.push(parsedData[i].data_hutang_umum[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_hutang_bank.length;z++){
        		data_hutang_bank.push(parsedData[i].data_hutang_bank[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_hutang_vendor.length;z++){
        		data_hutang_vendor.push(parsedData[i].data_hutang_vendor[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_hutang_vendor_due.length;z++){
        		data_hutang_vendor_due.push(parsedData[i].data_hutang_vendor_due[z]);	
        	}

        }

    Highcharts.setOptions({
     colors: ['#2CAFED','#FFA62D','#0CCB2C','ED561B']
    });

	Highcharts.chart('container_hutang_umum', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: '<strong>HUTANG</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['Bank : ' + data_hutang_umum[1].toString() + 'M', data_hutang_umum[1]],
	            ['Vendor : ' + data_hutang_umum[0].toString() + 'M', data_hutang_umum[0]],
	            ['Beban hrs dibayar : ' + data_hutang_umum[2].toString() + 'M', data_hutang_umum[2]],
	            ['Pajak : ' + data_hutang_umum[3].toString() + 'M', data_hutang_umum[3]]
	        ],
	        showInLegend: true
	    }]
	});


    // HUTANG BANK
	Highcharts.chart('container_hutang_bank', {
	    chart: {
	        type: 'pie',
	        options3d: {
	            enabled: true,
	            alpha: 45
	        }
	    },
	    title: {
	        text: '<strong>HUTANG BANK</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    plotOptions: {
	        pie: {
	            innerSize: 70,
	            depth: 25,
	            dataLabels: {
	                enabled: false
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Total Amount',
	        data: [
	            ['SKBDN : ' + data_hutang_bank[0].toString() + 'M', data_hutang_bank[0]],
	            ['AR : ' + data_hutang_bank[1].toString() + 'M', data_hutang_bank[1]],
	            ['Investasi : ' + data_hutang_bank[2].toString() + 'M', data_hutang_bank[2]]
	        ],
	        showInLegend: true
	    }]
	});

    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // HUTANG VENDOR AGING
	Highcharts.chart('container_hutang_vendor_aging', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<strong>MONITORING AGING HUTANG VENDOR</strong>'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['1-7', '8-14', '15-30', '31-60', '61-90','91-120','121-180','181-360','>361']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: data_hutang_vendor
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: data_hutang_vendor_due
	    }]
	});


//END
    }
});
    
}

function onchange_partner_id(val) {
	var partner_id = document.getElementById('sel_vendor').value;
	var data_start = 'none';
	var data_end = 'none';
	
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;

    var data_hutang_vendor = [];
    var data_hutang_vendor_due = [];

  $.ajax({
    url:"/data/keuangan/get_hutang_by_period/" + partner_id.toString(),
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){

        	for(var z = 0; z < parsedData[i].data_hutang_vendor.length;z++){
        		data_hutang_vendor.push(parsedData[i].data_hutang_vendor[z]);	
        	}

        	for(var z = 0; z < parsedData[i].data_hutang_vendor_due.length;z++){
        		data_hutang_vendor_due.push(parsedData[i].data_hutang_vendor_due[z]);	
        	}

        }


    Highcharts.setOptions({
     colors: ['#2CAFED','#D60006']
    });

    // HUTANG VENDOR AGING DETAIL (PER VENDOR)
	Highcharts.chart('container_hutang_vendor_aging_detail', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	        // text: '<strong>MONITORING AGING HUTANG BY VENDOR</strong>'
	    },
	    // subtitle: {
	    //     text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    // },
	    xAxis: {
	        categories: ['1-7 Hari', '8-14 Hari', '15-30 Hari', '31-60 Hari', '61-90 Hari','91-120 Hari','121-180 Hari','181-360 Hari','>361 Hari']
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total in M'
	        },
	        stackLabels: {
	            enabled: true,
	            style: {
	                fontWeight: 'bold',
	                color: ( // theme
	                    Highcharts.defaultOptions.title.style &&
	                    Highcharts.defaultOptions.title.style.color
	                ) || 'gray'
	            }
	        }
	    },
	    legend: {
	        align: 'right',
	        x: -30,
	        verticalAlign: 'top',
	        y: 70,
	        floating: true,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || 'white',
	        borderColor: '#CCC',
	        borderWidth: 1,
	        shadow: false
	    },
	    tooltip: {
	        headerFormat: '<b>{point.x}</b><br/>',
	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Belum Jatuh Tempo',
	        data: data_hutang_vendor
	    }, {
	        name: 'Sudah Jatuh Tempo',
	        data: data_hutang_vendor_due
	    }]
	});



//END
    }
});
    
}


