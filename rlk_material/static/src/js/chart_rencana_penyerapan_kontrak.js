window.onload = function() {
    
    $.ajax({
    url:"/data/get_info_pasar",
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);
        for(var i = 0; i < parsedData.length;i++){
            console.log('testing');
        }

        // KONTRAK EXISTING
        Highcharts.chart('container1', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            title: {
                text: 'Kontrak Eksiting'
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 165000,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'M3'
                },
                plotBands: [{
                    from: 0,
                    to: 60000,
                    color: '#DF5353' // red
                }, {
                    from: 60000,
                    to: 120000,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 120000,
                    to: 180000,
                    color: '#55BF3B' // green
                }]
            },
            credits: {
                  enabled: false
            },

            series: [{
                name: 'Volume',
                data: [99000],
                tooltip: {
                    valueSuffix: ' M3'
                }
            }]

        });
        // END OF CHART

        // RENCANA KONTRAK 
        Highcharts.chart('container2', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            title: {
                text: 'Rencana Kontrak'
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 165000,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'M3'
                },
                plotBands: [{
                    from: 0,
                    to: 60000,
                    color: '#DF5353' // red
                }, {
                    from: 60000,
                    to: 120000,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 120000,
                    to: 180000,
                    color: '#55BF3B' // green
                }]
            },
            credits: {
                  enabled: false
            },

            series: [{
                name: 'Volume',
                data: [31000],
                tooltip: {
                    valueSuffix: ' M3'
                }
            }]

        });
        // END OF CHART

        // TOTAL KONTRAK
        Highcharts.chart('container3', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            title: {
                text: 'Total Kontrak (Eksisting + Rencana)'
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 165000,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'M3'
                },
                plotBands: [{
                    from: 0,
                    to: 60000,
                    color: '#DF5353' // red
                }, {
                    from: 60000,
                    to: 120000,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 120000,
                    to: 180000,
                    color: '#55BF3B' // green
                }]
            },
            credits: {
                  enabled: false
            },

            series: [{
                name: 'Volume',
                data: [130000],
                tooltip: {
                    valueSuffix: ' M3'
                }
            }]

        });
        // END OF CHART
    }


    });
}


