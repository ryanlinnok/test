
//LOAD DATA
var total_produksi = [];
var total_realisasi = [];

$.ajax({
url:"/data/get_produksi",
success: function(data,status,jqXHR) {
  var parsedData = JSON.parse(data);
  for(var i = 0; i < parsedData.length;i++){
    total_produksi.push(parsedData[i].total);
  }
}
});


$.ajax({
url:"/data/get_realisasi",
success: function(data,status,jqXHR) {
  var parsedData = JSON.parse(data);
  for(var i = 0; i < parsedData.length;i++){
    total_realisasi.push(parsedData[i].total);
  }
}
});


//LOAD GRAPH AFTER JSON
$(document).ready(function(){

    var tahun = (new Date()).getFullYear()

    var ctx = document.getElementById("myChartProduksi").getContext('2d');

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan'+ '-' + tahun,'Feb'+ '-' + tahun,
                    'Mar'+ '-' + tahun,'Apr'+ '-' + tahun
                    ,'Mei'+ '-' + tahun,'Jun'+ '-' + tahun,
                    'Jul'+ '-' + tahun,'Agt'+ '-' + tahun,
                    'Sep'+ '-' + tahun,'Okt'+ '-' + tahun,
                    'Nop'+ '-' + tahun,'Des'+ '-' + tahun
                ],
            datasets: [{
                label: 'Produksi',
                data: total_produksi,
                backgroundColor: '#2e86c1',
                borderColor: '#2386c1',
                borderWidth: 1
            },


            {
                label: 'Realisasi',
                data: total_realisasi,
                backgroundColor:'#ae00ff',
                borderColor: '#ae00ff',
                borderWidth: 1
            }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    })


});