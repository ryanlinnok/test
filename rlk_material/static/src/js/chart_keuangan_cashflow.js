window.onload = function() {

    var rencana_cash_in = [];
    var rencana_cash_out = [];
    var realisasi_cash_in = [];
    var realisasi_cash_out = [];
    var x_label = [];

    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;


  $.ajax({
    url:"/data/keuangan/get_cashflow/",
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].rencana_cash_in.length;z++){
        		rencana_cash_in.push(parsedData[i].rencana_cash_in[z]);	
        	}

        	for(var z = 0; z < parsedData[i].rencana_cash_out.length;z++){
        		rencana_cash_out.push(parsedData[i].rencana_cash_out[z]);	
        	}
        	
        	for(var z = 0; z < parsedData[i].realisasi_cash_in.length;z++){
        		realisasi_cash_in.push(parsedData[i].realisasi_cash_in[z]);	
        	}

        	for(var z = 0; z < parsedData[i].realisasi_cash_out.length;z++){
        		realisasi_cash_out.push(parsedData[i].realisasi_cash_out[z]);	
        	}

        	for(var z = 0; z < parsedData[i].x_label.length;z++){
        		x_label.push(parsedData[i].x_label[z]);	
        	}
        }

    // Highcharts.setOptions({
    //  colors: ['#50B432','#ED561B']
    // });


    // CASHFLOW PERIODC
	Highcharts.chart('container_cashflow_periodic', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Cashflow Periodic'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['Saldo Awal', 'Cash In', 'Cash Out', 'Net Cashflow', 'Saldo Akhir', 'Saldo Kas/Bank'],
	        title: {
	            text: null
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total',
	            align: 'high'
	        },
	        labels: {
	            overflow: 'justify'
	        }
	    },
	    tooltip: {
	        valueSuffix: ' millions'
	    },
	    plotOptions: {
	        bar: {
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    legend: {
	        layout: 'horizontal',
	        align: 'center',
	        verticalAlign: 'bottom',
	        // x: -40,
	        y: -10,
	        floating: false,
	        // borderWidth: 1,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
	        shadow: false
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Rencana Teoritis',
	        data: [0, 127.44, 113.35, 0, 0, 0]
	    }, {
	        name: 'Rencana Revisi',
	        data: [0, 100, 87.66, 12.34, 0, 0]
	    }, {
	        name: 'Realisasi',
	        data: [1.09, 8.29, 7.66, 0.63, 1.72, 2]
	    }]

	});

    // CASH IN
	Highcharts.chart('container_cash_in', {

	    title: {
	        text: 'Rencana VS Realisasi Cash In'
	    },

	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },

	    xAxis: {
	        categories: x_label
	    },

	    yAxis: {
	        title: {
	            text: 'Total (Rp.)'
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },

	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            }
	        }
	    },

	    credits: {
	          enabled: false
	    },

	    series: [{
	        name: 'Rencana',
	        data: rencana_cash_in
	    }, {
	        name: 'Realisasi',
	        data: realisasi_cash_in
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }

	});

    Highcharts.chart('container_cash_out', {

	    title: {
	        text: 'Rencana VS Realisasi Cash Out'
	    },

	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },

	    xAxis: {
	        categories: x_label
	    },

	    yAxis: {
	        title: {
	            text: 'Total (Rp.)'
	        }
	    },

	    credits: {
	          enabled: false
	    },

	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },

	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	        }
	    },

	    series: [{
	        name: 'Rencana',
			color: '#3498DB',
	        data: rencana_cash_out
	    }, {
	        name: 'Realisasi',
	        color: '#E67E22',
	        data: realisasi_cash_out
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }

	});


//END
    }
});
}

function onchange_periode(val) {

	if(val == 'daily'){
		document.getElementById('sel_harian_start').style.visibility = "visible";
		document.getElementById('sel_harian_end').style.visibility = "visible";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";
	}
    else if(val == 'monthly'){
		document.getElementById('sel_harian_start').style.visibility = "hidden";
		document.getElementById('sel_harian_end').style.visibility = "hidden";
		document.getElementById('sel_bulanan_start').style.visibility = "visible";
		document.getElementById('sel_bulanan_end').style.visibility = "visible";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";

    }
    else if(val == 'yearly'){
		document.getElementById('sel_harian_start').style.visibility = "hidden";
		document.getElementById('sel_harian_end').style.visibility = "hidden";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "visible";
		document.getElementById('sel_tahunan_end').style.visibility = "visible";
    }
	else {
		document.getElementById('sel_harian_start').style.visibility = "visible";
		document.getElementById('sel_harian_end').style.visibility = "visible";
		document.getElementById('sel_bulanan_start').style.visibility = "hidden";
		document.getElementById('sel_bulanan_end').style.visibility = "hidden";
		document.getElementById('sel_tahunan_start').style.visibility = "hidden";
		document.getElementById('sel_tahunan_end').style.visibility = "hidden";
	}
    
}


function tampilkan(val) {
	var period = document.getElementById('sel1').value;
	var data_start = 'none';
	var data_end = 'none';
	
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0');
	var yyyy = today.getFullYear();
	today = dd + '/' + mm + '/' + yyyy;
	organisasi = document.getElementById('organisasi').value;



	if(period == 'daily'){
		if((document.getElementById('id_sel_harian_start').value == '') || (document.getElementById('id_sel_harian_end').value == '')){
			alert('Harap isi periode awal atau periode akhir');
		}
		else{
			data_start = document.getElementById('id_sel_harian_start').value
			data_end = document.getElementById('id_sel_harian_end').value
		}
	}
    else if(period == 'monthly'){
		if((document.getElementById('id_sel_bulanan_start').value == '') || (document.getElementById('id_sel_bulanan_end').value == '')){
			alert('Harap isi periode awal atau periode akhir');
		}
		else{
			data_start = document.getElementById('id_sel_bulanan_start').value
			data_end = document.getElementById('id_sel_bulanan_end').value
		}
    }
    else if(period == 'yearly'){
		if((document.getElementById('id_sel_tahunan_start').value == '') || (document.getElementById('id_sel_tahunan_end').value == '')){
			alert('Harap isi periode awal atau periode akhir');
		}
		else{
			data_start = document.getElementById('id_sel_tahunan_start').value
			data_end = document.getElementById('id_sel_tahunan_end').value
		}

    }

    var rencana_cash_in = [];
    var rencana_cash_out = [];
    var realisasi_cash_in = [];
    var realisasi_cash_out = [];
    var x_label = [];


  $.ajax({
    url:"/data/keuangan/get_cashflow/" + period.toString() + "/" + data_start.toString() + "/" + data_end.toString(),
    success: function(data,status,jqXHR) {
        var parsedData = JSON.parse(data);

        for(var i = 0; i < parsedData.length;i++){
        	for(var z = 0; z < parsedData[i].rencana_cash_in.length;z++){
        		rencana_cash_in.push(parsedData[i].rencana_cash_in[z]);	
        	}

        	for(var z = 0; z < parsedData[i].rencana_cash_out.length;z++){
        		rencana_cash_out.push(parsedData[i].rencana_cash_out[z]);	
        	}
        	
        	for(var z = 0; z < parsedData[i].realisasi_cash_in.length;z++){
        		realisasi_cash_in.push(parsedData[i].realisasi_cash_in[z]);	
        	}

        	for(var z = 0; z < parsedData[i].realisasi_cash_out.length;z++){
        		realisasi_cash_out.push(parsedData[i].realisasi_cash_out[z]);	
        	}

        	for(var z = 0; z < parsedData[i].x_label.length;z++){
        		x_label.push(parsedData[i].x_label[z]);	
        	}
        }


    // CASHFLOW PERIODC
	Highcharts.chart('container_cashflow_periodic', {
	    chart: {
	        type: 'bar'
	    },
	    title: {
	        text: 'Cashflow Periodic'
	    },
	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },
	    xAxis: {
	        categories: ['Saldo Awal', 'Cash In', 'Cash Out', 'Net Cashflow', 'Saldo Akhir', 'Saldo Kas/Bank'],
	        title: {
	            text: null
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Total',
	            align: 'high'
	        },
	        labels: {
	            overflow: 'justify'
	        }
	    },
	    tooltip: {
	        valueSuffix: ' millions'
	    },
	    plotOptions: {
	        bar: {
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    legend: {
	        layout: 'horizontal',
	        align: 'center',
	        verticalAlign: 'bottom',
	        // x: -40,
	        y: -10,
	        floating: false,
	        // borderWidth: 1,
	        backgroundColor:
	            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
	        shadow: false
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Rencana Teoritis',
	        data: [0, 127.44, 113.35, 0, 0, 0]
	    }, {
	        name: 'Rencana Revisi',
	        data: [0, 100, 87.66, 12.34, 0, 0]
	    }, {
	        name: 'Realisasi',
	        data: [1.09, 8.29, 7.66, 0.63, 1.72, 2]
	    }]

	});


    // CASH IN
	Highcharts.chart('container_cash_in', {

	    title: {
	        text: 'Rencana VS Realisasi Cash In'
	    },

	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },

	    xAxis: {
	        categories: x_label
	    },

	    yAxis: {
	        title: {
	            text: 'Total (Rp.)'
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },

	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            }
	        }
	    },

	    credits: {
	          enabled: false
	    },

	    series: [{
	        name: 'Rencana',
	        data: rencana_cash_in
	    }, {
	        name: 'Realisasi',
	        data: realisasi_cash_in
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }

	});

    Highcharts.chart('container_cash_out', {

	    title: {
	        text: 'Rencana VS Realisasi Cash Out'
	    },

	    subtitle: {
	        text: '<strong>'+organisasi+'</strong>' + '<br/>Per ' + today
	    },

	    xAxis: {
	        categories: x_label
	    },

	    yAxis: {
	        title: {
	            text: 'Total (Rp.)'
	        }
	    },

	    credits: {
	          enabled: false
	    },

	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },

	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            },
	        }
	    },

	    series: [{
	        name: 'Rencana',
			color: '#3498DB',
	        data: rencana_cash_out
	    }, {
	        name: 'Realisasi',
	        color: '#E67E22',
	        data: realisasi_cash_out
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }

	});


//END
    }
});
    
}




