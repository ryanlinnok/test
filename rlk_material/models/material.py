
# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError

class MaterialMaterial(models.Model):
    _name = 'material.material'
    _description = 'Material'

    name = fields.Char(string='Material Name', required=True, copy=True)
    code = fields.Char(string='Material Code', required=True, copy=True)
    type = fields.Selection(
        [('Pabric', 'Pabric'),
         ('Jeans', 'Jeans'),
         ('Cotton', 'Cotton')], string='Material Type', required=True, copy=True)
    price = fields.Float('Material Buy Price', required=True, copy=True)
    partner_id = fields.Many2one('res.partner', 'Supplier', required=True, copy=True)

    @api.constrains('price')
    def _check_validate_price(self):
        for rec in self:
            if rec.price < 100:
                raise ValidationError(_('Material Buy Price cannot be less than 100.'))


