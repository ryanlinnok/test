from odoo import models, fields, api
import odoo
from odoo import  http
from odoo.http import request


class ControllerMaterial(http.Controller):

    @http.route('/rlk_material/main', type='http', auth='user', website=True)
    def ControllerMaterialMain(self, **kw):
        data = {}
        material_list = []
        # uid = request.uid
        # material_ids = request.env['material.material'].search([('create_uid','=',uid)])
        partner_id = request.env.user.partner_id.id
        material_ids = request.env['material.material'].search([('partner_id','=',partner_id)])
        for rec in material_ids:
            material_list.append({
                'name' : rec.name,
                'code' : rec.code,
                'type' : rec.type,
                'price' : rec.price,
                'partner_id' : rec.partner_id.name
            })
        data = {'material_list' : material_list}
        return http.request.render('rlk_material.material_main_view', data)